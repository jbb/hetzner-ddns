// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#[macro_use]
extern crate serde;

mod config;
mod hdnsapi;
mod ipify;
use config::Config;
use hdnsapi::v1::{DNSApi, RecordType, UpdateRecord};
use ipify::{IpAddresses, IpifyAPI};

use reqwest::blocking::Client;

use std::time::Duration;

fn main() {
    let config = Config::open().expect("Failed to read config");
    let client = Client::new();
    let dns = DNSApi::new_with_client(config.auth.token, &client);
    let ipify = IpifyAPI::new_with_client(&client);

    let mut last_ip = IpAddresses::default();
    loop {
        match ipify.get_own_ip() {
            Ok(ip) => {
                if ip != last_ip {
                    if let Some(ref v4) = ip.v4 {
                        println!("Legacy IP Adress changed to {}", v4.clone());
                        let record = UpdateRecord {
                            value: v4.clone(),
                            ttl: 120,
                            record_type: RecordType::A,
                            name: config.a_record.name.clone(),
                            zone_id: config.a_record.zone_id.clone(),
                        };
                        match dns.update_record(&config.a_record.id, record) {
                            Ok(r) => println!("Legacy IP Record is now {:?}", r.record),
                            Err(e) => println!("Failed to update record: {}", e),
                        }
                    }

                    if let Some(ref v6) = ip.v6 {
                        if let Some(ref aaaa) = config.aaaa_record {
                            println!("IPv6 Address changed to {}", v6.clone());
                            let record = UpdateRecord {
                                value: v6.clone(),
                                ttl: 120,
                                record_type: RecordType::Aaaa,
                                name: aaaa.name.clone(),
                                zone_id: aaaa.zone_id.clone(),
                            };
                            match dns.update_record(&aaaa.id, record) {
                                Ok(r) => println!("IPv6 Record is now {:?}", r.record),
                                Err(e) => println!("Failed to update IPv6 record: {}", e),
                            }
                        }
                    }

                    last_ip = ip;
                }
            }
            Err(e) => {
                eprintln!("Failed to find out own IP address: {}", e)
            }
        }

        std::thread::sleep(Duration::from_secs(config.update_interval));
    }
}
