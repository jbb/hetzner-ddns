// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use reqwest::blocking::Client;

static IPIFY_IPV4_URL: &str = "https://api.ipify.org?format=json";
static IPIFY_IPV6_URL: &str = "https://api6.ipify.org?format=json";

pub struct IpifyAPI<'c> {
    client: &'c Client,
}

#[derive(Deserialize)]
struct IPResponse {
    ip: String,
}

#[derive(Debug, PartialEq, Eq, Default)]
pub struct IpAddresses {
    pub v4: Option<String>,
    pub v6: Option<String>,
}

impl<'c> IpifyAPI<'c> {
    pub fn new_with_client(client: &'c Client) -> IpifyAPI<'c> {
        IpifyAPI { client }
    }

    pub fn get_own_ip(&self) -> reqwest::Result<IpAddresses> {
        let v4 = self
            .client
            .get(IPIFY_IPV4_URL)
            .send()
            .ok()
            .and_then(|r| r.json::<IPResponse>().ok())
            .map(|r| r.ip);

        let v6 = self
            .client
            .get(IPIFY_IPV6_URL)
            .send()
            .ok()
            .and_then(|r| r.json::<IPResponse>().ok())
            .map(|r| r.ip);

        Ok(IpAddresses { v4, v6 })
    }
}
