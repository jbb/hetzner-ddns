// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub mod v1 {
    use reqwest::blocking::Client;

    const API_URL: &str = "https://dns.hetzner.com/api/v1";
    const AUTH_HEADER: &str = "Auth-API-Token";

    pub type UpdateRecord = CreateRecord;
    pub type UpdateResponse = CreateResponse;

    #[derive(Serialize, Deserialize, Debug)]
    #[serde(rename_all = "UPPERCASE")]
    pub enum RecordType {
        A,
        Aaaa,
        Ns,
        Mx,
        Cname,
        Rp,
        Txt,
        Soa,
        Hinfo,
        Srv,
        Dane,
        Tlsa,
        Ds,
        Caa,
    }

    #[derive(Deserialize, Debug)]
    pub struct Record {
        #[serde(rename = "type")]
        pub record_type: RecordType,
        pub id: String,
        pub created: String,
        pub modified: String,
        pub zone_id: String,
        pub name: String,
        pub ttl: i64,
    }

    #[derive(Deserialize)]
    struct Records {
        pub records: Vec<Record>,
    }

    #[derive(Serialize)]
    pub struct CreateRecord {
        pub value: String,
        pub ttl: i64,
        #[serde(rename = "type")]
        pub record_type: RecordType,
        pub name: String,
        pub zone_id: String,
    }

    #[derive(Deserialize, Debug)]
    pub struct CreateResponse {
        pub record: Record,
    }

    #[derive(Deserialize, Debug)]
    pub struct RecordsResponse {
        pub records: Vec<Record>,
    }

    pub struct DNSApi<'c> {
        pub token: String,
        pub client: &'c Client,
    }

    impl<'c> DNSApi<'c> {
        pub fn new_with_client(token: String, client: &'c Client) -> DNSApi<'c> {
            DNSApi { token, client }
        }

        #[allow(dead_code)]
        pub fn records(&self, zone_id: &str) -> reqwest::Result<RecordsResponse> {
            self.client
                .get(&format!("{}/records?zone_id={}", API_URL, zone_id))
                .header(AUTH_HEADER, &self.token)
                .send()?
                .json()
        }

        #[allow(dead_code)]
        pub fn create_records(
            &self,
            create_recod: CreateRecord,
        ) -> reqwest::Result<CreateResponse> {
            self.client
                .post(&format!("{}/records", API_URL))
                .header(AUTH_HEADER, &self.token)
                .json(&create_recod)
                .send()?
                .json()
        }

        #[allow(dead_code)]
        pub fn record(&self, record_id: &str) -> reqwest::Result<Record> {
            self.client
                .post(&format!("{}/records/{}", API_URL, record_id))
                .header(AUTH_HEADER, &self.token)
                .send()?
                .json()
        }

        #[allow(dead_code)]
        pub fn update_record(
            &self,
            record_id: &str,
            update_record: CreateRecord,
        ) -> reqwest::Result<UpdateResponse> {
            self.client
                .put(&format!("{}/records/{}", API_URL, record_id))
                .header(AUTH_HEADER, &self.token)
                .json(&update_record)
                .send()?
                .json()
        }

        #[allow(dead_code)]
        pub fn delete_record(&self, record_id: &str) -> reqwest::Result<()> {
            self.client
                .delete(&format!("{}/records/{}", API_URL, record_id))
                .header(AUTH_HEADER, &self.token)
                .send()?;

            Ok(())
        }
    }
}
