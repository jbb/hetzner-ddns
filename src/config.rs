#[derive(Serialize, Deserialize)]
pub struct Record {
    pub id: String,
    pub name: String,
    pub zone_id: String,
}

#[derive(Serialize, Deserialize)]
pub struct Auth {
    pub token: String,
}

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub update_interval: u64,
    pub a_record: Record,
    pub aaaa_record: Option<Record>,
    pub auth: Auth,
}

impl Config {
    pub fn open() -> anyhow::Result<Config> {
        let config = toml::from_str(&std::fs::read_to_string("/etc/hetzner-ddns/config.toml")?)?;
        Ok(config)
    }
}
